#include "pncl_lexer.h"

#include <stdio.h>


#include <fcntl.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/io.h>
#include <sys/mman.h>


#define MAX_TOKENS 2048


struct pncl_token tokens[MAX_TOKENS];



int
main(int argc, char **argv)
{
	int fd;

	struct stat file_stats;
	int file_size;

	const char *file_contents;

	fd = open(argv[1], O_RDONLY);

	fstat(fd, &file_stats);
	file_size = file_stats.st_size;

	file_contents = mmap(0, file_size, PROT_READ, MAP_PRIVATE, fd, 0);

	{

		struct pncl_lexer_state lexer;
		pncl_lexer_init(&lexer);

		int tokens_read;

		tokens_read = pncl_lexer_lex(&lexer,
		                             file_contents,
		                             file_size,
		                             tokens,
		                             MAX_TOKENS);
		
		printf("read %d tokens!\n", tokens_read);

		for (int i = 0; i < tokens_read; i++) {
			switch (tokens[i].type) {
			case PNCL_TOK_IDENTIFIER:
				printf("identifier: ");
				printf("%.*s\n", tokens[i].length_in_source,
				                 file_contents + tokens[i].offset_in_source);
				break;
			case PNCL_TOK_EOF:
				printf("eof.\n");
				break;
			case PNCL_TOK_INTEGER_LITERAL:
				printf("integer: %lld\n", tokens[i].integer_value);
				break;
			case PNCL_TOK_FLOATING_LITERAL:
				printf("float: %Lf\n", tokens[i].floating_value);
				break;
			default:
				printf("type: %d\n", tokens[i].type);
				break;
			}
		}

	}

	return 0;
}
