#include "pncl_lexer.h"

#define local static

/* -------- HELPER FUNCTIONS ---------- */

#define _PNCL_IS_WHITESPACE(c) (c == ' ' || c == '\t' || c == '\n' || c == '\r')

#define _PNCL_IS_ALPHA(c) ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
#define _PNCL_IS_NUM(c) (c >= '0' && c <= '9')
#define _PNCL_IS_HEX(c) (_PNCL_IS_NUM(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))

#define _PNCL_HEX_TO_INT(c) (c >= 'a' ? (c - 'a' + 10) : (c >= 'A' ? (c - 'A' + 10) : (c - '0')))
#define _PNCL_NUM_TO_INT(c) (c - '0')

local int
_pncl_allocate_token(struct pncl_lexer_state *lexer,
                     unsigned int tokens_size)
{
	if (lexer->allocated_tokens >= tokens_size) {
		return -1;
	}

	lexer->allocated_tokens += 1;
	return lexer->allocated_tokens - 1;
}


struct _pncl_source_position {
	unsigned int line;
	unsigned int col;
};


local struct _pncl_source_position
_pncl_get_source_position(struct pncl_lexer_state *lexer)
{
	struct _pncl_source_position position;

	position.line = lexer->line;
	position.col  = lexer->col;

	return position;
}


local char
_pncl_peek_char(struct pncl_lexer_state *lexer,
                     const char *source,
                     unsigned int source_size,
                     unsigned int lah)
{
	if (lexer->offset_in_source + lah >= source_size) {
		return -1;
	}

	return source[lexer->offset_in_source + lah];
}

local char
_pncl_read_next_char(struct pncl_lexer_state *lexer,
                     const char *source,
                     unsigned int source_size)
{
	char c;

	if (lexer->offset_in_source >= source_size) {
		return -1;
	}

	c = source[lexer->offset_in_source];
	lexer->offset_in_source += 1;
	
	if (c == '\n') {
		lexer->line += 1;
		lexer->col   = 1;
	} else {
		lexer->col += 1;
	}

	return c;
}

local void
_pncl_read_chars(struct pncl_lexer_state *lexer,
                 const char *source,
                 unsigned int source_size,
                 unsigned int num_chars)
{
	for (unsigned int i = 0; i < num_chars; i++) {
		_pncl_read_next_char(lexer,
		                     source,
		                     source_size);
	}
}

/* ------------- pncl API ------------- */
void
pncl_lexer_init(struct pncl_lexer_state *lexer)
{
	lexer->line = 1;
	lexer->col  = 1;
	
	lexer->offset_in_source = 0;
	lexer->allocated_tokens = 0;
}


int
pncl_lexer_lex(struct pncl_lexer_state *lexer,
               const char *source,
               unsigned int source_size,
               struct pncl_token *tokens,
               unsigned int tokens_size)
{
	int token;
	char c;

	int tokens_read = 0;

	while (42) {
		c = _pncl_peek_char(lexer,
		                    source,
		                    source_size,
		                    0);

		if (_PNCL_IS_WHITESPACE(c)) {
			_pncl_read_next_char(lexer,
			                     source,
			                     source_size);
			continue;
		}


		if (c == '/') { /* maybe a comment */
			c = _pncl_peek_char(lexer,
			                    source,
			                    source_size,
			                    1);
			if (c == '/') { /* line comment */
				while (c != '\n' && c != -1) {
					c = _pncl_read_next_char(lexer,
					                         source,
					                         source_size);
				}
				continue;
			} else if (c == '*') { /* block comment */
				unsigned int depth = 1;
			
				_pncl_read_chars(lexer,
				                 source,
				                 source_size,
				                 2);

				while (depth > 0) {
					c = _pncl_read_next_char(lexer,
					                         source,
					                         source_size);
					if (c == -1) {
						break;
					}

					if (c == '/') {
						c = _pncl_read_next_char(lexer,
						                         source,
						                         source_size);
						if (c == '*') {
							depth += 1;
						}

					} else if (c == '*') {
						c = _pncl_read_next_char(lexer,
						                         source,
						                         source_size);
						if (c == '/') {
							depth -= 1;
						}
					}
				}

				continue;
			}

			/* not a comment */
			c = _pncl_peek_char(lexer,
			                    source,
			                    source_size,
			                    0);
		}

		if (tokens_read == 0 && c == -1) {
			return 0; /* job finished */
		}

		token = _pncl_allocate_token(lexer, tokens_size);

		if (token == -1) {
			if (tokens_read == 0) {
				return -1;
			}
			return tokens_read;
		}

		
		struct _pncl_source_position start_pos;
		start_pos = _pncl_get_source_position(lexer);

		struct _pncl_source_position end_pos;


		tokens[token].offset_in_source = lexer->offset_in_source;
		tokens[token].position.line  = start_pos.line;
		tokens[token].position.col   = start_pos.col;

		switch (c) {
		case -1:
			/* we have read a few tokens before,
			 * so now is a good time to put a EOF token
			 */
			tokens[token].type = PNCL_TOK_EOF;
			return tokens_read + 1;

		default:
			if (_PNCL_IS_ALPHA(c) || c == '_') {
				/* identifier */

				tokens[token].length_in_source = 0;

				while (_PNCL_IS_ALPHA(c) || _PNCL_IS_NUM(c) || c == '_') {
					_pncl_read_next_char(lexer,
					                     source,
					                     source_size);
					tokens[token].length_in_source += 1;

					c = _pncl_peek_char(lexer,
					                    source,
					                    source_size,
					                    0);
				}

				tokens_read += 1;
				tokens[token].type = PNCL_TOK_IDENTIFIER;

				/* @TODO: keywords */
				continue;
			}

			if (c == '0') {
				/* number literal that might be base 16 */
				c = _pncl_peek_char(lexer,
				                    source,
				                    source_size,
				                    1);
				if (c == 'x') {
					_pncl_read_chars(lexer,
					                 source,
					                 source_size,
					                 2);

					c = _pncl_peek_char(lexer,
					                    source,
					                    source_size,
					                    0);
					tokens[token].length_in_source = 2;
					tokens[token].integer_value    = 0;

					while (_PNCL_IS_HEX(c) || c == '_') {

						tokens[token].length_in_source += 1;

						_pncl_read_chars(lexer,
						                 source,
						                 source_size,
						                 1);
						if (c != '_') {
							tokens[token].integer_value *= 16;
							tokens[token].integer_value |= _PNCL_HEX_TO_INT(c);
						}
						c = _pncl_peek_char(lexer,
						                    source,
						                    source_size,
						                    0);
					}
					
					tokens[token].type = PNCL_TOK_INTEGER_LITERAL;

					tokens_read += 1;
					continue;
				} else {
					c = '0';
				}
			}

			if (_PNCL_IS_NUM(c)) {

				tokens[token].length_in_source = 0;
				tokens[token].integer_value    = 0;

				while (_PNCL_IS_NUM(c) || c == '_') {

					tokens[token].length_in_source += 1;

					_pncl_read_chars(lexer,
					                 source,
					                 source_size,
					                 1);
					if (c != '_') {
						tokens[token].integer_value *= 10;
						tokens[token].integer_value += _PNCL_NUM_TO_INT(c);
					}
					c = _pncl_peek_char(lexer,
					                    source,
					                    source_size,
					                    0);
				}

				tokens[token].type = PNCL_TOK_INTEGER_LITERAL;

				if (c == '.') {
					_pncl_read_chars(lexer,
					                 source,
					                 source_size,
					                 1);
					c = _pncl_peek_char(lexer,
					                    source,
					                    source_size,
					                    0);

					unsigned long long whole_part = tokens[token].integer_value;
					long double fraction_point = 1.0;

					tokens[token].floating_value = whole_part;

					while (_PNCL_IS_NUM(c) || c == '_') {

						tokens[token].length_in_source += 1;

						_pncl_read_chars(lexer,
						                 source,
						                 source_size,
						                 1);
						if (c != '_') {
							tokens[token].floating_value *= 10;
							tokens[token].floating_value += _PNCL_NUM_TO_INT(c);
							fraction_point *= 10.0;
						}
						c = _pncl_peek_char(lexer,
						                    source,
						                    source_size,
						                    0);
					}

					tokens[token].floating_value /= fraction_point;

					tokens[token].type = PNCL_TOK_FLOATING_LITERAL;
				}
			
				tokens_read += 1;
				continue;
			}
		}

	}

	return 0;
}
