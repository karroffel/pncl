#ifndef PNCL_LEXER_H
#define PNCL_LEXER_H

/* the different types a token can be */
enum pncl_token_type {
	PNCL_TOK_IDENTIFIER,
	PNCL_TOK_INTEGER_LITERAL,
	PNCL_TOK_FLOATING_LITERAL,

	PNCL_TOK_OPERATOR,

	PNCL_TOK_PAREN_OPEN,
	PNCL_TOK_PAREN_CLOSE,
	PNCL_TOK_BRACE_OPEN,
	PNCL_TOK_BRACE_CLOSE,
	PNCL_TOK_CURLY_OPEN,
	PNCL_TOK_CURLY_CLOSE,

	PNCL_TOK_SEMICOLON,
	PNCL_TOK_COMMA,

	PNCL_TOK_EOF,

	PNCL_TOK_MAX
};

/* definition of a pncl token */
struct pncl_token {
	enum pncl_token_type type;

	unsigned int offset_in_source;
	unsigned int length_in_source;

	struct {
		unsigned int line;
		unsigned int col;
	} position;

	union {
		unsigned long long integer_value;

		long double floating_value;
	};
};


/* lexer state definition */
struct pncl_lexer_state {
	unsigned int line;
	unsigned int col;

	unsigned int offset_in_source;

	unsigned int allocated_tokens;
};



/* setting up the tokenizer state */
void
pncl_lexer_init(struct pncl_lexer_state *lexer);


/* lex tokens from the source code.
 * returns number of tokens read.
 *   0  means no more tokens to read. (aka "job is finished")
 *   -1 means that there was no space to allocate a new token
 */
int
pncl_lexer_lex(struct pncl_lexer_state *lexer,
               const char *source,
               unsigned int source_size,
               struct pncl_token *tokens,
               unsigned int tokens_size);


#endif
